package Hashing;


public class HashMap<K, V> {

	private Entry<K, V>[] entry_Table;
	private int initialCapacity = 16;

	public HashMap() {
		entry_Table = new Entry[initialCapacity];
	}
	public HashMap(int capacity) {
		initialCapacity = capacity;
		entry_Table = new Entry[initialCapacity];
	}

	public void put(K key, V value) {
		if (key == null) {
			throw new RuntimeException("null key is not allowed");
		}
		// hash value of the key
		int hashValue = hashValue(key);
		// create the entry
		Entry<K, V> entry = new Entry<K, V>(key, value, null);

		// if no entry found at the hash value index of the entry table then put
		// the value
		if (entry_Table[hashValue] == null) {
			entry_Table[hashValue] = entry;
		} else {// if found then put the value in a linked list
			Entry<K, V> previous = null;
			Entry<K, V> current = entry_Table[hashValue];
			while (current != null) {
				if (current.k.equals(key)) {
					if (previous == null) {
						entry.next = current.next;
						entry_Table[hashValue] = entry;
					} else {
						entry.next = current.next;
						previous.next = entry;
					}
				}
				previous = current;
				current = current.next;
			}
			previous.next = entry;
		}

	}

	public V get(K key) {
		if (key == null) {
			return null;
		}
		// hash value of the key
		int hashValue = hashValue(key);
		if (entry_Table[hashValue] == null) {
			return null;
		} else {
			Entry<K, V> temp = entry_Table[hashValue];
			while (temp != null) {
				if (temp.k.equals(key)) {
					return temp.v;
				}
				temp = temp.next;
			}
		}
		return null;
	}

        //If  any entry is found then simply de-link the node from linked list and return true.

	public boolean remove(K key) {
		if (key == null) {
			return false;
		}
		// hash value of the key
		int hashValue = hashValue(key);
		if (entry_Table[hashValue] == null) {
			return false;
		} else {
			Entry<K, V> previous = null;
			Entry<K, V> current = entry_Table[hashValue];
			while (current != null) {
				if (current.k.equals(key)) {
					if (previous == null) {
						entry_Table[hashValue] = entry_Table[hashValue].next;
						return true;
					} else {
						previous.next = current.next;
						return true;
					}
				}
				previous = current;
				current = current.next;
			}
			return false;
		}
	}

//checks if any entry is present in the entry table.

	public boolean containsKey(K key) {
		int hashValue = hashValue(key);
		if (entry_Table[hashValue] == null) {
			return false;
		} else {
			Entry<K, V> current = entry_Table[hashValue];
			while (current != null) {
				if (current.k.equals(key)) {
					return true;
				}
				current = current.next;
			}
		}
		return false;
	}


// Here  considered node counts of each linked list as well as vertical count for each bucket location.
	public int size() {
		int count = 0;
		for (int i = 0; i < entry_Table.length; i++) {
			if (entry_Table[i] != null) {
				int nodeCount = 0;
				for (Entry<K, V> e = entry_Table[i]; e.next != null; e = e.next) {
					nodeCount++;
				}
				count += nodeCount;
				count++;// consider also vertical count
			}
		}
		return count;
	}

        
	private int hashValue(K key) {
		return Math.abs(key.hashCode()) % initialCapacity;
	}

	private static class Entry<K, V> {
		private K k;
		private V v;
		private Entry<K, V> next;

		public Entry(K k, V v, Entry<K, V> next) {
			this.k = k;
			this.v = v;
			this.next = next;
		}

	}

}



