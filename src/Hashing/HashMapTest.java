package Hashing;

import java.util.HashMap;
public class HashMapTest {

	public static void main(String[] args) {
                    HashMap<String, Double> map = new HashMap<>(1);
		map.put("1", 122.0);
		map.put("2", 12.0);

		System.out.println("Contains Key 1: " + map.containsKey("1"));
		map.put("3", 2.0);

		System.out.println("Contains Key 3: " + map.containsKey("3"));


		HashMap<String,Double> map2 = new HashMap<>(1);
		map2.put("V", 122.0);
                map2.put("M", 666.0);

                System.out.println("Get Key V: " + map2.get("V"));
		System.out.println("Get Key M: " + map2.get("M"));
		
	}

}
